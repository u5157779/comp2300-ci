FROM ubuntu:16.04
RUN apt update && apt -y install python python3 python-pip python3-pip git
RUN pip install -U PlatformIO
# auto script python3 package dependencies
RUN pip3 install -U pygdbmi jsonschema pylint pyyaml
# clone assignment 1 template and install pio packages
RUN mkdir -p /tmp/pio-tmp; cd /tmp/pio-tmp
RUN pio init
RUN printf '[env:disco_l476vg]\nplatform = ststm32@4.0.1\nframework = stm32cube\nboard = disco_l476vg\n' > platformio.ini
RUN printf '.syntax unified\n.global main\n.type main, %%function\nmain:\n  nop\n' > src/main.S
RUN pio run
RUN pio platform install ststm32@4.0.1 --with-package tool-stm32duino --with-package tool-stlink --with-package tool-openocd
