import sys
import os

import argparse
from urllib.request import urlopen
import urllib
import yaml
import json
import jsonschema


def parse_cmdargs():
    parser = argparse.ArgumentParser()

    parser.add_argument("--exists", action="store_true",
        help="check the existance of a file")
    parser.add_argument("--schema", type=str,
        help="specify the URL to a schema against which the YAML file is valided.")
    parser.add_argument("file", help="path to file")
    return parser.parse_args()


def check_file_exists(path: str) -> int:
    if os.path.exists(path):
        return 0
    print("Error: file '%s' not found. Have you added it and pushed it?" % path)
    return 1


def check_yaml_schema(path: str, schema_url: str) -> int:
    with open(path, "r") as fp:
        try:
            yaml_obj = yaml.load(fp)
        except (yaml.scanner.ScannerError, yaml.parser.ParserError) as e:
            print("Invalid YAML format: %s" % e)
            return 1

        with urlopen(schema_url) as response:
            schema = json.loads(response.read().decode('utf-8'))
            try:
                jsonschema.validate(yaml_obj, schema)
            except jsonschema.ValidationError as e:
                if "does not match" in e.message:
                    print("Validation failed: '%s' does not match expected format." % e.instance)
                else:
                    print("Validation failed: %s" % e.message)
                print("Path: %s" % " -> ".join(map(str, e.relative_path)))
                return 1

def main():
    args = parse_cmdargs()

    if args.exists:
        return check_file_exists(args.file)
    else:
        return check_yaml_schema(args.file, args.schema)

if __name__ == "__main__":
    sys.exit(main())